# frozen_string_literal: true

class SetLocaleMiddleware
  # BEGIN
  def initialize(app)
    @app = app
  end

  def call(env)
    I18n.locale = extract_locale_from_accept_language_header(env)
    @app.call(env)
  end

  private
    def extract_locale_from_accept_language_header(env)
      req = Rack::Request.new(env)
      header = req.env.fetch('HTTP_ACCEPT_LANGUAGE', '')
      locale = header.scan(/^[a-z]{2}/).first || ''

      return I18n.default_locale unless I18n.available_locales.include?(locale.to_sym)
      locale
    end
  # END
end
