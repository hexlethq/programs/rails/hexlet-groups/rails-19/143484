# frozen_string_literal: true

def censor(word, stop_words)
  censor = '$#%!'
  word = censor if stop_words.include? word
  word
end

def make_censored(text, stop_words)
  # BEGIN
  text
    .split
    .map { |word| censor(word, stop_words) }
    .join(' ')
  # END
end
