# frozen_string_literal: true

# BEGIN
def prepare_version(version)
  version.split('.').map(&:to_i)
end

def compare_versions(version1, version2)
  v1_major, v1_minor = prepare_version(version1)
  v2_major, v2_minor = prepare_version(version2)

  compared_major = v1_major <=> v2_major
  compared_minor = v1_minor <=> v2_minor

  compared_major.zero? ? compared_minor : compared_major
end
# END
