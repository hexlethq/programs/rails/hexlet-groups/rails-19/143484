# frozen_string_literal: true

# BEGIN
def build_query_string(params)
  params
    .sort_by { |k| [k] }
    .map { |key, value| "#{key}=#{value}" }
    .join('&')
end
# END
