# frozen_string_literal: true

require_relative 'test_helper'
require_relative '../lib/stack'

class StackTest < Minitest::Test
  # BEGIN
  def setup
    @stack = Stack.new([1, 2, 3])
  end

  def test_pop
    @stack.pop!
    assert_equal @stack.to_a, [1, 2]
  end

  def test_push
    @stack.push! 4
    assert_equal @stack.to_a, [1, 2, 3, 4]
  end

  def test_empty
    empty_stack = Stack.new([])
    assert empty_stack.empty?
  end

  def test_to_a
    assert_equal @stack.to_a.class, Array
  end

  def test_clear
    assert_equal @stack.empty?, false
    @stack.clear!
    assert_empty @stack.to_a
  end

  def test_size
    assert_equal @stack.size, 3
  end
  # END
end

test_methods = StackTest.new({}).methods.select do |method|
  method.to_s.start_with? 'test_'
end

raise 'StackTest has not tests!' if test_methods.empty?
