# frozen_string_literal: true

require 'application_system_test_case'

# BEGIN
class PostsTest < ApplicationSystemTestCase
  test 'visiting the root' do
    visit root_path

    assert_selector 'h1', text: 'Home#index'
  end

  test 'visiting the index' do
    visit posts_path

    assert_selector 'h1', text: 'Posts'
  end

  test 'creating a post' do
    visit posts_path

    click_on 'New Post'

    fill_in 'Title', with: 'My new post'
    fill_in 'Body', with: 'some body'
    click_on 'Create Post'
  
    assert_text 'Post was successfully created.'
  end

  test 'updating a post' do
    visit posts_path

    click_on 'Edit', match: :first

    fill_in 'Body', with: 'new body'
    click_on 'Update Post'
    
    assert_text 'Post was successfully updated.'
  end

  test 'destroying a post' do
    visit posts_path

    page.accept_confirm do
      click_on 'Destroy', match: :first
    end

    assert_text 'Post was successfully destroyed.'
  end

  test 'creating a post_comment' do
    visit posts_path

    click_on 'Show', match: :first

    fill_in 'post_comment_body', with: 'new comment'
    click_on 'Create Comment'
  
    assert_text 'Comment was successfully created.'
  end
end
# END
