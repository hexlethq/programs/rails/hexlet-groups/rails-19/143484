# frozen_string_literal: true

require 'test_helper'

class PostFlowTest < ActionDispatch::IntegrationTest
  test 'can see the welcome page' do
    get root_url
    assert_select 'h1', 'Home#index'
  end

  test 'can create an post' do
    get new_post_url
    assert_response :success

    assert_difference 'Post.count', 1 do
      post posts_url, params: {
        post: {
          title: 'can create',
          body: 'post successfully.'
        }
      }
    end

    assert_response :redirect

    follow_redirect!

    assert_response :success

    assert_select 'h1', 'can create'
  end
end
