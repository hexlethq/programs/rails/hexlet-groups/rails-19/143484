require "test_helper"

class BulletinsControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get bulletins_path
    assert_response :success
  end

  test "should get show" do
    get bulletin_path bulletins.first
    assert_response :success
  end
end