# frozen_string_literal: true

# BEGIN

# END
# frozen_string_literal: true

require 'uri'
require 'forwardable'

# extension URI
class Url
  extend Forwardable
  include Comparable

  def initialize(url)
    @url = URI(url)
  end

  def_delegators :@url, :scheme, :host, :to_s

  def query_params
    @url
      .query
      .split('&')
      .each_with_object({}) do |param, hash|
        key, value = param.split('=')
        hash[key.to_sym] = value
      end
  end

  def query_param(key, default_key = nil)
    query_params.fetch(key, default_key)
  end

  def <=>(other)
    @url.to_s <=> other.to_s
  end
end
