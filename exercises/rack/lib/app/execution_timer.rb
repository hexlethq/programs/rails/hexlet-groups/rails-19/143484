# frozen_string_literal: true

require 'rack'

class ExecutionTimer
  def initialize(app)
    @app = app
  end

  def call(env)
    request_started_on = Time.now
    status, headers, body = @app.call(env)
    request_ended_on = Time.now

    resp_time = ((request_ended_on - request_started_on) * 1000).round(4)
    execution_time = %(Response time: #{resp_time} ms)
    body << execution_time

    [status, headers, body]
  end
end
