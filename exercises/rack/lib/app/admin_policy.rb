# frozen_string_literal: true

# middleware
class AdminPolicy
  def initialize(app)
    @app = app
  end

  def call(env)
    req = Rack::Request.new(env)
    if req.path_info.start_with? '/admin'
      [403, {}, ['']]
    else
      @app.call(env)
    end
  end
end
