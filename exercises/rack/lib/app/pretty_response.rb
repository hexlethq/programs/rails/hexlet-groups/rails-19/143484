# frozen_string_literal: true

class PrettyResponse
  def initialize(app)
    @app = app
  end

  def call(env)
    status, headers, body = @app.call(env)
    pretty_body = body.join("\n")
    [status, headers, [pretty_body]]
  end
end
