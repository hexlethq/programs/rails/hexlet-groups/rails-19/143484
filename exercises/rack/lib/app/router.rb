# frozen_string_literal: true

require 'rack'

class Router
  # rubocop:disable Metrics/MethodLength
  def call(env)
    req = Rack::Request.new(env)
    status = 200
    headers = { 'Content-Type' => 'text/plain' }

    case req.path_info
    when '/'
      body = 'Hello, World!'
    when '/about'
      body = 'About page'
    else
      body = '404 Not Found'
      status = 404
    end

    [status, headers, [body]]
  end
  # rubocop:enable Metrics/MethodLength
end
