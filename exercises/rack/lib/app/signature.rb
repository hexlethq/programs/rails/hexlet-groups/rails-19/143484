# frozen_string_literal: true

require 'digest'
require 'rack'

# encrypt body
class Signature
  def initialize(app)
    @app = app
  end

  def call(env)
    status, headers, body = @app.call(env)
    encrypted_body = Digest::SHA2.new(256).hexdigest body.first
    body << encrypted_body

    [status, headers, body]
  end
end
