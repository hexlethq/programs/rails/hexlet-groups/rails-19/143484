# frozen_string_literal: true

require 'rack'
require_relative 'app/admin_policy'
require_relative 'app/execution_timer'
require_relative 'app/signature'
require_relative 'app/router'
require_relative 'app/pretty_response'

module App
  def self.init
    Rack::Builder.new do |builder|
      builder.use PrettyResponse
      builder.use AdminPolicy
      builder.use ExecutionTimer
      builder.use Signature

      builder.run Router.new
    end
  end
end
