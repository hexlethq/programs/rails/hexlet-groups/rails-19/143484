# frozen_string_literal: true

# BEGIN
def get_fizz_buzz_by_num(num)
  if (num % 3).zero? && (num % 5).zero?
    'FizzBuzz'
  elsif (num % 3).zero?
    'Fizz'
  elsif (num % 5).zero?
    'Buzz'
  else
    num.to_s
  end
end

def fizz_buzz(start, stop)
  result = ''
  return result if start > stop

  range = (start..stop).to_a
  range.each do |num|
    result = "#{result} #{get_fizz_buzz_by_num(num)}"
  end

  result.strip
end
# END
