# frozen_string_literal: true

# BEGIN
def iter(index)
  return index if index <= 1

  iter(index - 1) + iter(index - 2)
end

def fibonacci(index)
  return nil if index.negative?
  return 0 if index <= 1

  iter(index - 1)
end

# END
