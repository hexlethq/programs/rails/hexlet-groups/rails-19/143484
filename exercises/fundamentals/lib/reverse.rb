# frozen_string_literal: true

# BEGIN
def reverse(str)
  reversed_str = ''
  str.each_char do |ch|
    reversed_str = "#{ch}#{reversed_str}"
  end

  reversed_str
end
# END
