# frozen_string_literal: true

require 'date'

# BEGIN
module Model
  def self.included(base)
    base.extend ClassMethods
  end

  attr_reader :attributes

  def initialize(attributes = {})
    @attributes = self.class.schema.each_with_object({}) do |(attr_name, options), res|
      source_type = options[:type]
      default_value = options[:default]
      value = attributes.fetch(attr_name, default_value)
      res[attr_name] = convert source_type, value
    end
  end

  private

  def convert(source_type, value)
    return nil if value.nil?

    converter = {
      integer: ->(val) { val.to_i },
      string: ->(val) { val.to_s },
      datetime: ->(val) { DateTime.parse val },
      boolean: ->(val) { to_boolean val }
    }[source_type]

    converter.call(value)
  end

  def to_boolean(val)
    case val
    when true, 'yes', 'true' then true
    when false, 'no', 'false' then false
    else
      raise ArgumentError, "invalid value for convert boolean(): #{val}"
    end
  end

  # ClassMethods
  module ClassMethods
    def schema
      @schema ||= {}
    end

    def attribute(attr_name, options)
      schema[attr_name] = options

      define_method attr_name do
        @attributes[attr_name]
      end

      define_method "#{attr_name}=" do |value|
        source_type = self.class.schema[attr_name][:type]
        @attributes[attr_name] = convert source_type, value
      end
    end
  end
end
# END
