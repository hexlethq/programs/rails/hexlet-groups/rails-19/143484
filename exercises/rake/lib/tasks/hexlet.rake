require 'csv'

namespace :hexlet do
  desc "Import users from csv into database"
  task :import_users, [:path_to_users] => :environment do |_, args|
    path_to_users = args.fetch(:path_to_users, nil)
    if path_to_users.nil?
      puts 'please, pass path to users.csv file'
      exit
    end

    CSV.foreach(path_to_users, headers: true) do |row|
      user = row.to_h
      User.create user
    end
  end
end
