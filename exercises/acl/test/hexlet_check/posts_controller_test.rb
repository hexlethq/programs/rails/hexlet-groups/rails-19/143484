# frozen_string_literal: true

require 'test_helper'

module HexletCheck
  class PostsControllerTest < ActionDispatch::IntegrationTest
    self.use_instantiated_fixtures = true

    setup do
      @post = posts(:one)
    end

    test 'should get index' do
      get posts_url
      assert_response :success
    end

    test 'guest should raise error from new' do
      assert_raises(Pundit::NotAuthorizedError) do
        get new_post_url
      end
    end

    test 'signin user should get new' do
      sign_in_as :one

      get new_post_url
      assert_response :success
    end

    test 'guest cant create post' do
      user = users :one
      assert_no_difference('Post.count') do
        assert_raises(Pundit::NotAuthorizedError) do
          post posts_url, params: { post: {
            title: 'Title',
            body: 'Body',
            user_id: user.id
          } }
        end
      end
    end

    test 'signin user can create post' do
      user = users :one
      sign_in_as :one

      assert_difference('Post.count') do
        post posts_url, params: { post: {
          title: 'Title',
          body: 'Body',
          user_id: user.id
        } }
      end

      assert_redirected_to post_url(Post.last)
    end

    test 'should show post' do
      get post_url(@post)
      assert_response :success
    end

    test 'author can edit' do
      sign_in_as :one

      get edit_post_url(@post)
      assert_response :success
    end

    test 'admin can edit' do
      sign_in_as :admin

      get edit_post_url(@post)
      assert_response :success
    end

    test 'non author cant get edit' do
      sign_in_as :two

      assert_raises(Pundit::NotAuthorizedError) do
        get edit_post_url(@post)
      end
    end

    test 'author can update post' do
      sign_in_as :one

      patch post_url(@post), params: { post: {
        title: 'Title',
        body: 'Body'
      } }
      assert_redirected_to post_url(@post)
    end

    test 'not author cant update post' do
      assert_raises(Pundit::NotAuthorizedError) do
        patch post_url(@post), params: { post: {
          title: 'Title',
          body: 'Body'
        } }
      end

      assert @post.title != 'Title'
    end

    test 'only admin can destroy post' do
      sign_in_as :admin

      assert_difference('Post.count', -1) do
        delete post_url(@post)
      end

      assert_redirected_to posts_url
    end

    test 'non admin cant destroy post' do
      sign_in_as :one
      assert_no_difference('Post.count') do
        assert_raises(Pundit::NotAuthorizedError) do
          delete post_url(@post)
        end
      end
    end
  end
end
