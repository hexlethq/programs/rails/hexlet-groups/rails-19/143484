# frozen_string_literal: true

class PostsController < ApplicationController
  before_action :set_post, only: %i[show edit update destroy]

  after_action :verify_authorized, except: %i[index show]

  # BEGIN
  def new
    @post = Post.new
    authorize @post
  end

  def index
    @posts = Post.all
  end

  def show; end

  def edit
    authorize @post  
  end

  def create
    authorize Post

    @post = current_user.posts.build(post_params)
    if @post.save
      redirect_to @post, notice: 'Post was successfully created.'
    else
      render :new, status: :unprocessable_entity
    end
  end

  def update
    authorize @post

    if @post.update(post_params)
      redirect_to @post, notice: 'Post was successfully updated.'
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    authorize @post

    @post.destroy
    
    redirect_to posts_url, notice: 'Post was successfully destroyed.'
  end
  # END

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_post
    @post = Post.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def post_params
    params.require(:post).permit(:title, :body)
  end
end
