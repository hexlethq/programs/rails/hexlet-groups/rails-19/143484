class Posts::CommentsController < ApplicationController
  before_action :set_post
  before_action :set_comment, only: %i[ edit update destroy ]

  def edit
  end

  def create
    @comment = @post.comments.build(comment_params)
    if @comment.save
      redirect_to @post, notice: 'Comment was successfully created.'
    else
      render 'posts/show', status: :unprocessable_entity
    end
  end

  def update
    if @comment.update(comment_params)
      redirect_to @post, notice: "Post comment was successfully updated."
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    if @comment.destroy
      redirect_to @post, notice: "Post comment was successfully destroyed."
    else
      render 'posts/show', status: :unprocessable_entity
    end
  end

  private
    def set_comment
      @comment = @post.comments.find(params[:id])
    end

    def set_post
      @post = Post.find(params[:post_id])
    end

    def comment_params
      params.require(:post_comment).permit(:body)
    end
end
