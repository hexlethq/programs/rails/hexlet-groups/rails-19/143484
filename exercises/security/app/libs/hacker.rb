# frozen_string_literal: true

require 'open-uri'
require 'nokogiri'
require 'net/http'
require 'net/https'

# rubocop:disable Metrics/AbcSize
class Hacker
  class << self
    BASE_URL = 'https://rails-l4-collective-blog.herokuapp.com'

    def hack(email, password)
      sign_up_form_uri = URI.join(BASE_URL, 'users/', 'sign_up')
      sign_up_form_res = URI.open(sign_up_form_uri)
      cookie = sign_up_form_res.metas['set-cookie'].first
      html = Nokogiri::HTML(sign_up_form_res)
      # csrf_token = html.search('[@name="csrf-token"]').first['content']
      auth_token = html.search('[@name="authenticity_token"]').first['value']

      options = { :cookie => cookie, :auth_token => auth_token, :email => email, :password => password }
      sign_up_response = sign_up(options: options)

      case sign_up_response
      when Net::HTTPRedirection then
        :success
      when Net::HTTPSuccess then
        :user_alredy_exist
      else
        :failed
      end
    end

    private

    def sign_up(options:)
      cookie, auth_token, email, password = options.values_at(:cookie, :auth_token, :email, :password)

      sign_up_uri = URI.join(BASE_URL, 'users')
      https = Net::HTTP.new(sign_up_uri.host, sign_up_uri.port)
      https.use_ssl = true

      req = Net::HTTP::Post.new(sign_up_uri.path)
      form_data = {
        'authenticity_token' => auth_token,
        'user[email]' => email,
        'user[password]' => password,
        'user[password_confirmation]' => password,
        'commit' => 'Регистрация'
      }
      req.set_form_data(form_data)
      req['Cookie'] = cookie

      https.request(req)
    end
  end
end
# rubocop:enable Metrics/AbcSize
