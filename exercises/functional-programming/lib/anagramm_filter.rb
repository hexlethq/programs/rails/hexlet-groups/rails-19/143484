# frozen_string_literal: true

# BEGIN
def make_hash(word)
  word
    .chars
    .map(&:to_sym)
    .each_with_object({}) do |(char), hash|
      hash[char] = hash.fetch(char, 0) + 1
    end
end

def anagramm_filter(source_word, words)
  return [] if words.empty?

  source_word_hash = make_hash(source_word)
  words.filter do |word|
    word_hash = make_hash(word)
    word_hash == source_word_hash
  end
end
# END
