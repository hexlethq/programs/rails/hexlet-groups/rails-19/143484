# frozen_string_literal: true

# BEGIN
def get_same_parity(list)
  return [] if list.empty?

  first_el_parity = list.first % 2
  list.filter { |el| el % 2 == first_el_parity }
end
# END
