# frozen_string_literal: true

require 'date'

# BEGIN
def count_by_years(users)
  users
    .filter { |user| user[:gender] == 'male' }
    .each_with_object({}) do |(user), hash|
      birthday_date = Date.parse user[:birthday]
      birthday_year = birthday_date.year.to_s
      hash[birthday_year] = hash.fetch(birthday_year, 0) + 1
    end
end
# END
