class PagesController < ApplicationController
  def show
    @page_title = params[:id].split('_').last.capitalize
  end
end
